.. index:: Credits

	   
Credits
*******

:program:`dynasor` has been originally developed by Mattias Slabanja
in the Materials and Surface Theory division at the Department of
Physics at Chalmers University of Technology. Copyright and credit
belong to him. The documentation including the examples was written by
Erik Fransson.


Contact
-------

* Erik Fransson, erikfr AT chalmers.se
* Göran Wahnström, goran.wahnstrom AT chalmers.se
* Paul Erhart, erhart AT chalmers.se
