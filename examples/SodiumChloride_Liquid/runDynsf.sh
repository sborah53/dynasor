#!/bin/sh

DYNASOR=dynasor

K_MAX=30     # Consider kspace from gamma (0) to K_MAX inverse nanometer
K_BINS=100      # Collect result using K_BINS "bins" between 0 and $K_MAX
TIME_WINDOW=1000  # Consider time correlations up to TIME_WINDOW trajectory frames
MAX_FRAMES=50000   # Read at most MAX_FRAMES frames from trajectory file (then stop)

dt=$((5*4)) # This needs to be correspond to lammps timestep * dumpFreq * $STEP.

TRAJECTORY="lammpsrun/dump/dumpT1400.NVT.atom.velocity"
INDEXFILE="index.8"
OUTPUT="outputs/dynasor_out"


${DYNASOR} -f "$TRAJECTORY" -n "$INDEXFILE" \
    --k-bins=$K_BINS \
	--k-max=$K_MAX \
	--max-frames=$MAX_FRAMES \
	--nt=$TIME_WINDOW \
	--dt=$dt \
   --om=$OUTPUT.m







